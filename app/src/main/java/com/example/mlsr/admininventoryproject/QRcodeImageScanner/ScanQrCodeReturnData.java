package com.example.mlsr.admininventoryproject.QRcodeImageScanner;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;
import android.widget.Toolbar;

import com.example.mlsr.admininventoryproject.ConsumeInventoryItemsData.ConsumeItemsData;
import com.example.mlsr.admininventoryproject.DashboardPage.DashboardActivity;
import com.example.mlsr.admininventoryproject.InsertInventoryItemOptions.ItemUpdationActivity;
import com.example.mlsr.admininventoryproject.R;
import com.google.android.gms.vision.barcode.Barcode;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import info.androidhive.barcode.BarcodeReader;

public class ScanQrCodeReturnData extends AppCompatActivity implements BarcodeReader.BarcodeReaderListener {

    ArrayList<String> barCodesScannedData;
    CircleImageView finishScanButton;

    String intentValue;
    String ItemPassed;

//    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_qr_code_return_data);

        barCodesScannedData = new ArrayList<String>();
        barCodesScannedData.add("scanned data");

        finishScanButton = (CircleImageView) findViewById(R.id.finishScanButton);



        finishScanButtonClick();
//        toolbar = (Toolbar) findViewById(R.id.toolbar);
//        toolbar.setTitle("Bar code scanner");
//        toolbar.setTitleTextColor(Integer.parseInt("#ffffff"));

    }

    public void finishScanButtonClick(){
        finishScanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String s1 = "add Item";
                String s2 = "consume Item";
                Intent intent1 = getIntent();
                Bundle Bd = intent1.getExtras();
                if(Bd != null) {
                    ItemPassed = (String) Bd.get("ItemType");
                }
//                Intent intent = new Intent(ScanQrCodeReturnData.this, DashboardActivity.class);
//                Intent intent = new Intent(ScanQrCodeReturnData.this, ItemUpdationActivity.class);
                if(ItemPassed.equals(s1)) {
                    Intent intent = new Intent(ScanQrCodeReturnData.this, ItemUpdationActivity.class);
                    intent.putStringArrayListExtra("BarcodeData", barCodesScannedData);
                    startActivity(intent);
                }
                else{
                    Intent intent = new Intent(ScanQrCodeReturnData.this, ConsumeItemsData.class);
                    intent.putStringArrayListExtra("BarcodeData", barCodesScannedData);
                    startActivity(intent);
                }
            }
        });
    }

    @Override
    public void onScanned(Barcode barcode) {
        String result ="hi";
        int i=0;
        while(i < barCodesScannedData.size()) {
            if ( barCodesScannedData.get(i).equals(barcode.displayValue)) {
                break;
            }
            else{
                ++i;
            }
        }
        if ( i == barCodesScannedData.size()){
            barCodesScannedData.add(barcode.displayValue);
        }

        String op = "";
        for( String e: barCodesScannedData) {
            op += e + " " +"\n";
        }
    }

    @Override
    public void onScannedMultiple(List<Barcode> barcodes) {

    }

    @Override
    public void onBitmapScanned(SparseArray<Barcode> sparseArray) {

    }

    @Override
    public void onScanError(String errorMessage) {

    }

    @Override
    public void onCameraPermissionDenied() {

    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.navigation,menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        int id = item.getItemId();
//
//        if(id == R.id.action_back) {
//            Intent i = new Intent(getApplicationContext(),LoginActivity.class);
//            i.putStringArrayListExtra("data",barCodesScannedData);
//            startActivity(i);
////            Intent intent = new Intent(getApplicationContext(),LoginActivity.class);
////            intent.putStringArrayListExtra("barcode",barCodesScannedData);
////            setResult(RESULT_OK,intent);
////            startActivity(intent);
//            Toast.makeText(getApplicationContext(),"scanning has been stopped", Toast.LENGTH_SHORT).show();
//            return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }
}
