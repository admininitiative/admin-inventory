package com.example.mlsr.admininventoryproject.Interfaces;

import com.example.mlsr.admininventoryproject.ConsumeInventoryItemsData.Model.GetItemByQRCode;
import com.example.mlsr.admininventoryproject.ConsumeInventoryItemsData.Model.WarehouseData;
import com.example.mlsr.admininventoryproject.Models.PojoClasses.LoginData;
import com.example.mlsr.admininventoryproject.Models.PojoClasses.UpdateWareHouseItem;
import com.example.mlsr.admininventoryproject.Models.PojoClasses.WareHouseDetails;
import com.example.mlsr.admininventoryproject.Models.PojoClasses.WareHouseItemData;

import java.util.ArrayList;
import java.util.List;


import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by MlSr on 4/18/2018.
 */

public interface APIserviceInterface
{

    @GET("Login/Logon")
    Observable<LoginData> getLoginDetails(@Query("username") String username);

    @GET("Warehouse/GetWarehouseData")
    Observable<List<WareHouseDetails>> getWareHouseDetails();

    @GET("WareHouse/GetAllItemData")
    Observable<List<WareHouseItemData>> getAllItemDataOfWareHouse();

    @POST("Warehouse/UpdateItem")
    Observable<Response<String>> UpdateItemsToWareHouse(@Body UpdateWareHouseItem updateWareHouseItem);

    @GET("Warehouse/GetItemByQRParameter")
    Observable<List<GetItemByQRCode>> getItemByQRparameter(@Query("WarehouseName") String WareHouseName,@Query("Address") String address);

    @GET("Warehouse/GetWarehouseData")
    Observable<List<WarehouseData>> getAllWareHouseData();

}
