package com.example.mlsr.admininventoryproject.Models.PojoClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by MlSr on 5/18/2018.
 */

public class  UpdateWareHouseItem {

    @SerializedName("itemID")
    @Expose
    private Integer itemID;
    @SerializedName("WarehouseID")
    @Expose
    private Integer warehouseID;
    @SerializedName("WarehouseName")
    @Expose
    private String warehouseName;
    @SerializedName("ItemName")
    @Expose
    private String itemName;
    @SerializedName("Quantity")
    @Expose
    private Integer quantity;
    @SerializedName("MeasurementID")
    @Expose
    private Integer measurementID;
    @SerializedName("UnitType")
    @Expose
    private String unitType;
    @SerializedName("AddItem")
    @Expose
    private Boolean addItem;
    @SerializedName("SubtractItem")
    @Expose
    private Boolean subtractItem;

    public Integer getItemID() {
        return itemID;
    }

    public void setItemID(Integer itemID) {
        this.itemID = itemID;
    }

    public Integer getWarehouseID() {
        return warehouseID;
    }

    public void setWarehouseID(Integer warehouseID) {
        this.warehouseID = warehouseID;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getMeasurementID() {
        return measurementID;
    }

    public void setMeasurementID(Integer measurementID) {
        this.measurementID = measurementID;
    }

    public String getUnitType() {
        return unitType;
    }

    public void setUnitType(String unitType) {
        this.unitType = unitType;
    }

    public Boolean getAddItem() {
        return addItem;
    }

    public void setAddItem(Boolean addItem) {
        this.addItem = addItem;
    }

    public Boolean getSubtractItem() {
        return subtractItem;
    }

    public void setSubtractItem(Boolean subtractItem) {
        this.subtractItem = subtractItem;
    }

    public UpdateWareHouseItem(Integer itemID, Integer warehouseID, String warehouseName, String itemName, Integer quantity, Integer measurementID, String unitType, Boolean addItem, Boolean subtractItem) {
        this.itemID = itemID;
        this.warehouseID = warehouseID;
        this.warehouseName = warehouseName;
        this.itemName = itemName;
        this.quantity = quantity;
        this.measurementID = measurementID;
        this.unitType = unitType;
        this.addItem = addItem;
        this.subtractItem = subtractItem;
    }
}
