package com.example.mlsr.admininventoryproject.LoginPage;

import android.app.Activity;
import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.mlsr.admininventoryproject.ApiServices.ApiServiceBase;
import com.example.mlsr.admininventoryproject.DashboardPage.DashboardActivity;
import com.example.mlsr.admininventoryproject.Interfaces.APIserviceInterface;
import com.example.mlsr.admininventoryproject.Models.PojoClasses.LoginData;
import com.example.mlsr.admininventoryproject.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


public class LoginActivity extends AppCompatActivity {

    ImageView imageView;
    TextInputLayout UserNameEditText;
    TextInputLayout PasswordEditText;
    private ProgressBar progressBar;


    @BindView(R.id.Login_page_login_button)
    Button LoginButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

     //   LoginButton = (Button) findViewById(R.id.Login_page_login_button);

        imageView = (ImageView) findViewById(R.id.logo_imageView);
        UserNameEditText = (TextInputLayout) findViewById(R.id.Login_Page_username);
        PasswordEditText = (TextInputLayout) findViewById(R.id.LoginPage_password);
        progressBar = (ProgressBar) findViewById(R.id.progressBar1);

        UserNameEditText.setErrorEnabled(false);

        imageView.setImageResource(R.drawable.sample_log);

        TouchScreen();

        LoginButtonClick();
        progressBar.setVisibility(View.GONE);

    }

    public void TouchScreen(){
        findViewById(android.R.id.content).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideSoftKeyboard(LoginActivity.this);
                return false;
            }
        });
    }

    public void LoginButtonClick(){
        LoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                progressBar.setVisibility(View.VISIBLE);
                String Username = UserNameEditText.getEditText().getText().toString();
                APIserviceInterface apiService = ApiServiceBase.getAPIServiceInterfaceControl();
                Observable<LoginData> observable =  apiService.getLoginDetails(Username).subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread());


                observable.subscribe(new Observer<LoginData>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(LoginData loginData) {

                        String name = loginData.getUserName().toString().trim();
                        String password = loginData.getPassword().toString().trim();
                        String UserName = UserNameEditText.getEditText().getText().toString();
                        String UserPassword = PasswordEditText.getEditText().getText().toString();
                    //    String UserName = "Sreekar.Ml@in.fujitsu.com";
                        if(password.equals(UserPassword)) {

                            Intent intent = new Intent(LoginActivity.this, DashboardActivity.class);
                            startActivity(intent);
                            progressBar.setVisibility(View.GONE);
                            UserNameEditText.getEditText().setText("");
                            PasswordEditText.getEditText().setText("");
                        }
                        else{
                            Toast.makeText(getApplicationContext(),"Password Incorrect , Please Check",Toast.LENGTH_SHORT).show();
                            UserNameEditText.getEditText().setText("");
                            PasswordEditText.getEditText().setText("");
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                        Toast.makeText(getApplicationContext(), "Username Incorrect Please Check", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onComplete() {

                        Log.i("complete","complete");
                    }
                });


          }
        });
    }

    public void checkSignIn(){

    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), 0);
    }
}
