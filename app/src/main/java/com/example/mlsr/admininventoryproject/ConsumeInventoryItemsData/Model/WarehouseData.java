
package com.example.mlsr.admininventoryproject.ConsumeInventoryItemsData.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WarehouseData {

    @SerializedName("WarehouseID")
    @Expose
    private Integer warehouseID;
    @SerializedName("WarehouseName")
    @Expose
    private String warehouseName;
    @SerializedName("Address")
    @Expose
    private String address;
    @SerializedName("QRCode")
    @Expose
    private Object qRCode;
    @SerializedName("Status")
    @Expose
    private Object status;
    @SerializedName("PageNumber")
    @Expose
    private Integer pageNumber;

    public Integer getWarehouseID() {
        return warehouseID;
    }

    public void setWarehouseID(Integer warehouseID) {
        this.warehouseID = warehouseID;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Object getQRCode() {
        return qRCode;
    }

    public void setQRCode(Object qRCode) {
        this.qRCode = qRCode;
    }

    public Object getStatus() {
        return status;
    }

    public void setStatus(Object status) {
        this.status = status;
    }

    public Integer getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(Integer pageNumber) {
        this.pageNumber = pageNumber;
    }

}
