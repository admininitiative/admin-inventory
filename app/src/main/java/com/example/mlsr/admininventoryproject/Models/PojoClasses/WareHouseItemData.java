package com.example.mlsr.admininventoryproject.Models.PojoClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by MlSr on 5/8/2018.
 */

public class WareHouseItemData {

    @SerializedName("itemID")
    @Expose
    private Integer itemID;
    @SerializedName("WarehouseID")
    @Expose
    private Integer warehouseID;
    @SerializedName("ItemName")
    @Expose
    private String itemName;
    @SerializedName("Quantity")
    @Expose
    private Integer quantity;
    @SerializedName("MeasurementID")
    @Expose
    private Integer measurementID;

    public Integer getItemID() {
        return itemID;
    }

    public void setItemID(Integer itemID) {
        this.itemID = itemID;
    }

    public Integer getWarehouseID() {
        return warehouseID;
    }

    public void setWarehouseID(Integer warehouseID) {
        this.warehouseID = warehouseID;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getMeasurementID() {
        return measurementID;
    }

    public void setMeasurementID(Integer measurementID) {
        this.measurementID = measurementID;
    }

    public WareHouseItemData() {
    }

    public WareHouseItemData(Integer itemID, Integer warehouseID, String itemName, Integer quantity, Integer measurementID) {
        this.itemID = itemID;
        this.warehouseID = warehouseID;
        this.itemName = itemName;
        this.quantity = quantity;
        this.measurementID = measurementID;
    }
}
