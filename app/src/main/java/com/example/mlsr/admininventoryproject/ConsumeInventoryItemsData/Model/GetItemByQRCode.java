
package com.example.mlsr.admininventoryproject.ConsumeInventoryItemsData.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetItemByQRCode {

    @SerializedName("ItemData")
    @Expose
    private ItemData itemData;
    @SerializedName("WarehouseData")
    @Expose
    private WarehouseData warehouseData;
    @SerializedName("MeasurementData")
    @Expose
    private MeasurementData measurementData;

    public ItemData getItemData() {
        return itemData;
    }

    public void setItemData(ItemData itemData) {
        this.itemData = itemData;
    }

    public WarehouseData getWarehouseData() {
        return warehouseData;
    }

    public void setWarehouseData(WarehouseData warehouseData) {
        this.warehouseData = warehouseData;
    }

    public MeasurementData getMeasurementData() {
        return measurementData;
    }

    public void setMeasurementData(MeasurementData measurementData) {
        this.measurementData = measurementData;
    }

}
