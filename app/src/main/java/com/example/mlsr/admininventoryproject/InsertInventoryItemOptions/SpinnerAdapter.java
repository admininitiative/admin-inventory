package com.example.mlsr.admininventoryproject.InsertInventoryItemOptions;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.mlsr.admininventoryproject.Models.PojoClasses.WareHouseItemData;
import com.example.mlsr.admininventoryproject.R;

import java.util.ArrayList;

/**
 * Created by MlSr on 5/15/2018.
 */

public class SpinnerAdapter extends RecyclerView.Adapter<SpinnerAdapter.SingleTextView> {

//    ArrayList<String> namesArray;
//    ArrayList<WareHouseItemData> wareHouseItemDataArrayList;
    ArrayList<String> ItemIdData;
    Context context;
    InterfacePassData interfacePassData;
    String type;

    public SpinnerAdapter(ArrayList<String> ItemIdData, Context context,DialogDisplayFragment dialogDisplayFragment,String type) {

//        this.wareHouseItemDataArrayList = wareHouseItemDataArrayList;
        this.ItemIdData = ItemIdData;
        this.context = context;
        interfacePassData = dialogDisplayFragment;
        this.type = type;
    }

    @Override
    public SingleTextView onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.spinner_item_value,parent,false);
        SingleTextView singleTextView = new SingleTextView(v);
        return singleTextView;
    }

    @Override
    public void onBindViewHolder(final SingleTextView holder, final int position) {
//        holder.Spinner_item.setText(wareHouseItemDataArrayList.get(position).getItemID().toString());
        holder.Spinner_item.setText(ItemIdData.get(position).toString());

        holder.Spinner_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch(type){
                    case "item_id" : interfacePassData.itemID(ItemIdData.get(position).toString());
                        interfacePassData.closeDialog();
                    break;

                    case "ware_house_id" : interfacePassData.setWareHouseID(ItemIdData.get(position).toString());
                        interfacePassData.closeDialog();
                    break;

                    case "item_name":interfacePassData.setitemName(ItemIdData.get(position).toString());
                        interfacePassData.closeDialog();
                    break;

                    case "measurement":interfacePassData.setMeasurementType(ItemIdData.get(position).toString());
                        interfacePassData.closeDialog();
                    break;
                    default:
                        break;
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return ItemIdData.size();
    }

    public class SingleTextView extends RecyclerView.ViewHolder{
        private TextView Spinner_item;
        public SingleTextView(View itemView) {
            super(itemView);
            Spinner_item = (TextView) itemView.findViewById(R.id.spinner_display_id);
        }
    }

    public interface InterfacePassData {
        public void itemID(String name);
        public void setWareHouseID(String wareHouseId);
        public void setitemName(String itemName);
        public void setMeasurementType(String Type);
        public void closeDialog();
    }
}
