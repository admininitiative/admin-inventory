package com.example.mlsr.admininventoryproject.ApiServices;

import com.example.mlsr.admininventoryproject.Interfaces.APIserviceInterface;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by MlSr on 4/18/2018.
 */

public class ApiServiceBase {
    public static final String API_URl = "http://10.34.42.163/InventoryAPI/api/";

    public static Retrofit getRxRetrofitInstance(){
        return new Retrofit.Builder().baseUrl(API_URl)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    public static APIserviceInterface getAPIServiceInterfaceControl()
    {
        return getRxRetrofitInstance().create(APIserviceInterface.class);
    }
}
