package com.example.mlsr.admininventoryproject.ConsumeInventoryItemsData;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.example.mlsr.admininventoryproject.ApiServices.ApiServiceBase;
import com.example.mlsr.admininventoryproject.ConsumeInventoryItemsData.Model.WarehouseData;
import com.example.mlsr.admininventoryproject.Interfaces.APIserviceInterface;
import com.example.mlsr.admininventoryproject.Models.PojoClasses.WareHouseItemData;
import com.example.mlsr.admininventoryproject.R;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class DialogDispFragment extends Dialog implements CustomDialogAdapter.interfacePassDataToSetText {

    APIserviceInterface apiServiceInterface;
    RecyclerView DisplayDialogFragmentRecyView;
    Activity mActivity;
    interfacePassDataToConsumeItemsData interfaceConsumeItemsData;
    String type;
    ArrayList<WarehouseData> WareHouseDataList;
    


    public DialogDispFragment(Activity mActivity, String type) {
        super(mActivity);

       this.type = type;
       this.mActivity = mActivity;
       interfaceConsumeItemsData = (interfacePassDataToConsumeItemsData) mActivity;


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog_disp_fragment);

        DisplayDialogFragmentRecyView = (RecyclerView) findViewById(R.id.dispDialogFragmentRecyclerView);
        DisplayDialogFragmentRecyView.setLayoutManager(new LinearLayoutManager(mActivity));
        DisplayDialogFragmentRecyView.setHasFixedSize(true);

        LoadCompleteWareHouseData();

        LoadOnlywareHouseData();


    }

    public void LoadOnlywareHouseData(){
        apiServiceInterface = ApiServiceBase.getAPIServiceInterfaceControl();
        
        Observable<List<WarehouseData>> observable = apiServiceInterface.getAllWareHouseData().subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread());
        
        observable.subscribe(new Observer<List<WarehouseData>>() {
            
            @Override
            public void onSubscribe(Disposable d) {
                
            }

            @Override
            public void onNext(List<WarehouseData> warehouseData) {
                WareHouseDataList = new ArrayList<>();
                WareHouseDataList = (ArrayList<WarehouseData>) warehouseData;
                if(type == "ware_house_id_name") {
                    createDataCallAdapter(type, null, WareHouseDataList);
                }

            }

            @Override
            public void onError(Throwable e) {
                Log.i("warehouse data error", "ware house error");
            }

            @Override
            public void onComplete() {

            }
        });
    }

    public void LoadCompleteWareHouseData(){

        apiServiceInterface = ApiServiceBase.getAPIServiceInterfaceControl();

        Observable<List<WareHouseItemData>> observableWareHouseItemData = apiServiceInterface.getAllItemDataOfWareHouse().subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread());

        observableWareHouseItemData.subscribe(new Observer<List<WareHouseItemData>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(List<WareHouseItemData> wareHouseItemData) {
                ArrayList<WareHouseItemData> listOfWareHouseData = new ArrayList<WareHouseItemData>();
                listOfWareHouseData =(ArrayList<WareHouseItemData>) wareHouseItemData;
                if(type == "measurement" || type == "item_id" || type == "item_name") {
                    createDataCallAdapter(type, listOfWareHouseData,null);
                }
                
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    public void createDataCallAdapter(String type, ArrayList<WareHouseItemData> listOfWareHouseData,ArrayList<WarehouseData> WareHouseDataList){
        switch(type){
            case "item_id" :

                ArrayList<String> item_id_data = new ArrayList<>();
                item_id_data.add("ITEM ID LIST");
                for(int i=0;i<listOfWareHouseData.size();i++){
                    item_id_data.add(listOfWareHouseData.get(i).getItemID().toString());
                }
                CustomDialogAdapter customDialogAdapter = new CustomDialogAdapter(item_id_data,getContext(),DialogDispFragment.this,type);
                DisplayDialogFragmentRecyView.setAdapter(customDialogAdapter);

                break;

            case "ware_house_id_name" :

                ArrayList<String> warehouse_id_name_data = new ArrayList<>();
                warehouse_id_name_data.add(" WARE HOUSE ID LIST ");
                for(int i=0;i<WareHouseDataList.size();i++){
                //    warehouse_id_name_data.add(listOfWareHouseData.get(i).getWarehouseID().toString());
                    warehouse_id_name_data.add(WareHouseDataList.get(i).getWarehouseName().toString());
                }

                CustomDialogAdapter customDialogAdapter1 = new CustomDialogAdapter(warehouse_id_name_data,getContext(),DialogDispFragment.this,type);
                DisplayDialogFragmentRecyView.setAdapter(customDialogAdapter1);

//                SpinnerAdapter spinnerAdapter1 = new SpinnerAdapter(warehouse_id_name_data,getContext(),DialogDisplayFragment.this,type);
//                SpinnerItemRecyclerView.setAdapter(spinnerAdapter1);
                break;

            case "item_name" :

                ArrayList<String> item_name_data = new ArrayList<>();
                item_name_data.add("ITEM NAMES LIST");
                for(int i=0;i<listOfWareHouseData.size();i++){
                    item_name_data.add(listOfWareHouseData.get(i).getItemName().toString());
                }

                CustomDialogAdapter customDialogAdapter2 = new CustomDialogAdapter(item_name_data,getContext(),DialogDispFragment.this,type);
                DisplayDialogFragmentRecyView.setAdapter(customDialogAdapter2);

                break;

            case "measurement" :

                ArrayList<String> measurement_data = new ArrayList<>();
//                for(int i=0;i<listOfWareHouseData.size();i++){
//                    measurement_data.add(listOfWareHouseData.get(i).getMeasurementID().toString());
//                }
                measurement_data.add("MEASURENMENT TYPES");
                measurement_data.add("1");
                measurement_data.add("2");
                measurement_data.add("3");


                CustomDialogAdapter customDialogAdapter3 = new CustomDialogAdapter(measurement_data,getContext(),DialogDispFragment.this,type);
                DisplayDialogFragmentRecyView.setAdapter(customDialogAdapter3);

                break;
        }
    }

    @Override
    public void ItemId(String itemId) {
        interfaceConsumeItemsData.setItemId(itemId);
    }

    @Override
    public void WareHouseId(String wareHouseId) {
        interfaceConsumeItemsData.setWareHouseId(wareHouseId);
    }

    @Override
    public void ItemName(String itemName) {
        interfaceConsumeItemsData.setItemName(itemName);
    }

    @Override
    public void MeasurementType(String measurementType) {
        interfaceConsumeItemsData.setMeasurementType(measurementType);
    }

    @Override
    public void CloseDialog() {
        interfaceConsumeItemsData.closeDialog();
    }

    public interface interfacePassDataToConsumeItemsData {
        public void setItemId(String itemId);
        public void setWareHouseId(String wareHouseId);
        public void setItemName(String itemName);
        public void setMeasurementType(String measurementType);
        public void closeDialog();
    }
}
