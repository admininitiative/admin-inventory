package com.example.mlsr.admininventoryproject.DashboardPage;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mlsr.admininventoryproject.Models.PojoClasses.WareHouseDetails;
import com.example.mlsr.admininventoryproject.R;

import java.util.ArrayList;

/**
 * Created by MlSr on 4/24/2018.
 */

public class WareHouseDataListAdapter extends RecyclerView.Adapter<WareHouseDataListAdapter.SingleWareHouseDatatAdapter>{

    private Context context;
    private ArrayList<WareHouseDetails> wareHouseDetailsArrayList;
    WarehouseData warehouseData;

    public WareHouseDataListAdapter(Context context,WarehouseData warehouseData, ArrayList<WareHouseDetails> wareHouseDetailsArrayList) {
        this.context = context;
        this.wareHouseDetailsArrayList = wareHouseDetailsArrayList;
        this.warehouseData = warehouseData;
    }

    @Override
    public SingleWareHouseDatatAdapter onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_warehouse_record_item,parent,false);
        SingleWareHouseDatatAdapter singleWareHouseDatatAdapter = new SingleWareHouseDatatAdapter(v);
        return singleWareHouseDatatAdapter;
    }

    @Override
    public void onBindViewHolder(SingleWareHouseDatatAdapter holder, int position) {

        holder.WareNameTextView.setText(wareHouseDetailsArrayList.get(position).getWarehouseName().toString());
        final int wareHouseId = wareHouseDetailsArrayList.get(position).getWarehouseID();
        holder.WareNameTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*DashboardActivity dashboardActivity = new DashboardActivity();
                dashboardActivity.inventoriesOfWareHouse(wareHouseId);*/
                warehouseData.getWarehouseID(wareHouseId);
                Toast.makeText(context.getApplicationContext(),"hi i'm admin ware_house",Toast.LENGTH_LONG).show();

            }
        });
    }


    @Override
    public int getItemCount() {
        return wareHouseDetailsArrayList.size();
    }

    public class SingleWareHouseDatatAdapter extends RecyclerView.ViewHolder {

        private TextView WareNameTextView;
        public SingleWareHouseDatatAdapter(View itemView) {
            super(itemView);

            WareNameTextView = (TextView) itemView.findViewById(R.id.ware_house_name);

        }
    }

    public interface WarehouseData{
        void getWarehouseID(int warehouseID);
    }
}
