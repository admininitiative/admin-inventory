package com.example.mlsr.admininventoryproject.InsertInventoryItemOptions;

import android.app.Dialog;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.example.mlsr.admininventoryproject.ApiServices.ApiServiceBase;
import com.example.mlsr.admininventoryproject.ConsumeInventoryItemsData.Model.GetItemByQRCode;
import com.example.mlsr.admininventoryproject.Interfaces.APIserviceInterface;
import com.example.mlsr.admininventoryproject.LoginPage.LoginActivity;
import com.example.mlsr.admininventoryproject.Models.PojoClasses.UpdateWareHouseItem;
import com.example.mlsr.admininventoryproject.Models.PojoClasses.WareHouseItemData;
import com.example.mlsr.admininventoryproject.R;

import org.w3c.dom.Text;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

public class ItemUpdationActivity extends AppCompatActivity implements DialogDisplayFragment.AddEdittextdata{

    EditText edit_text_select_item;
    EditText edit_text_select_warehouse;
    EditText edit_text_item_name;
    EditText edit_text_item_quantity;
    EditText edit_text_select_measurement;
    Spinner select_item_Spinner;

    ImageButton button_show_item_id;
    ImageButton button_show_warehouse_id;
    ImageButton button_show_item_name;
    ImageButton button_show_measurement;

    Button UpdateItemButton;

//  ArrayList<WareHouseItemData> listOfWareHouseData;


//    public ItemUpdationActivity() {
//
//    }
    DialogDisplayFragment displayFragment;
    FragmentManager fragmentManager;
    APIserviceInterface apiService;

    ImageView imageViewProfilelogout;
    TextView logoutTextView;
    Dialog myDialog;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_item_updation);


        myDialog = new Dialog(this);
        myDialog.setContentView(R.layout.custom_pop_up);
        logoutTextView =(TextView) myDialog.findViewById(R.id.logout_button_text_view);


        edit_text_select_item = (EditText) findViewById(R.id.item_updation_select_item_id);
        edit_text_select_warehouse = (EditText) findViewById(R.id.item_updation_select_warehouse_id);
        edit_text_item_name = (EditText) findViewById(R.id.item_updation_item_name);
        edit_text_item_quantity = (EditText) findViewById(R.id.item_updation_item_quantity);
        edit_text_select_measurement = (EditText) findViewById(R.id.item_updation_measurement_id);

        imageViewProfilelogout = (ImageView) findViewById(R.id.add_item_image_view);

        UpdateItemButton = (Button) findViewById(R.id.update_items_button);

        button_show_item_id = (ImageButton) findViewById(R.id.select_item_id_drop_down);
        button_show_warehouse_id = (ImageButton) findViewById(R.id.select_warehouse_id_drop_down);
        button_show_item_name = (ImageButton) findViewById(R.id.select_item_name_drop_down);
        button_show_measurement = (ImageButton) findViewById(R.id.select_measurement_drop_down);

//        select_item_Spinner = (Spinner) findViewById(R.id.item_spinner);
//        fragmentManager = getFragmentManager();
        SpinnerOnClickListener();
        updateNewItemDetails();
        customDialogDisplayPopUp();
        textViewOnClikFunc();
        setAllBasedOnQRCodeDataScan();
    }

    public void setAllBasedOnQRCodeDataScan(){

        apiService = ApiServiceBase.getAPIServiceInterfaceControl();

        String WareHouseName = "1B1029";
        String WareHouseAddress = "B Wings , 1st floor, Old Building, Pune";

        Observable<List<GetItemByQRCode>> ObservableData = apiService.getItemByQRparameter(WareHouseName,WareHouseAddress).subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread());

        ObservableData.subscribe(new Observer<List<GetItemByQRCode>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(List<GetItemByQRCode> getItemByQRCodes) {
                edit_text_item_name.setText(getItemByQRCodes.get(0).getItemData().getItemName().toString());
                edit_text_item_quantity.setText(getItemByQRCodes.get(0).getItemData().getItemID().toString());
                edit_text_select_warehouse.setText(getItemByQRCodes.get(0).getWarehouseData().getWarehouseName().toString());
                edit_text_item_quantity.setText("10");
                edit_text_select_measurement.setText(getItemByQRCodes.get(0).getMeasurementData().getUnitType().toString());
            }

            @Override
            public void onError(Throwable e) {
                Log.i("Error ","error in item updation");
            }

            @Override
            public void onComplete() {

            }
        });
    }

    public void hideWindow(){
        View view = this.getCurrentFocus();
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public void customDialogDisplayPopUp(){
        imageViewProfilelogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.show();
            }
        });
    }

    public void textViewOnClikFunc(){
        logoutTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
                Intent intent = new Intent(ItemUpdationActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });
    }


    public void updateNewItemDetails() {
        UpdateItemButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String TempItemId = edit_text_select_item.getText().toString();
                String TempWareHouseId = edit_text_select_warehouse.getText().toString();
                String TempItemName = edit_text_item_name.getText().toString();
                String TempItemQty = edit_text_item_quantity.getText().toString();
                String TempMeaurement = edit_text_select_measurement.getText().toString();

                if(!TempItemId.isEmpty() && !TempWareHouseId.isEmpty() && !TempItemName.isEmpty() && !TempItemQty.isEmpty() && !TempMeaurement.isEmpty())
                {
                    int itemId = Integer.parseInt(edit_text_select_item.getText().toString());
                    int wareHouseId = Integer.parseInt(edit_text_select_warehouse.getText().toString());
                    String itemName = edit_text_item_name.getText().toString();
                    int itemQty = Integer.parseInt(edit_text_item_quantity.getText().toString());
                    int measurement = Integer.parseInt(edit_text_select_measurement.getText().toString());

//                WareHouseItemData wareHouseItemData = new WareHouseItemData(itemId,wareHouseId,itemName,itemQty,measurement);

                    UpdateWareHouseItem updateWareHouseItem = new UpdateWareHouseItem(itemId, wareHouseId, "admin", itemName, itemQty, measurement, "box", true, false);
                    apiService = ApiServiceBase.getAPIServiceInterfaceControl();

                    try {

                        io.reactivex.Observable<Response<String>> observable = apiService.UpdateItemsToWareHouse(updateWareHouseItem).subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread());

                        observable.subscribe(new Observer<Response<String>>() {
                            @Override
                            public void onSubscribe(Disposable d) {

                            }

                            @Override
                            public void onNext(Response<String> stringResponse) {
                                Toast.makeText(getApplicationContext(),"its working",Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onError(Throwable e) {

                            }

                            @Override
                            public void onComplete() {

                            }
                        });

                    }
                    catch(Exception ex) {
                        Toast.makeText(getApplicationContext(),"please enter all fields data",Toast.LENGTH_SHORT).show();
                    }

                }
                else{
                    Toast.makeText(getApplicationContext(),"please fill all fields",Toast.LENGTH_LONG).show();
                }
            }
        });
    }


            public void SpinnerOnClickListener() {

                button_show_item_id.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        displayFragment = new DialogDisplayFragment(ItemUpdationActivity.this, "item_id");
                        displayFragment.show();
                    }
                });

                button_show_warehouse_id.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        displayFragment = new DialogDisplayFragment(ItemUpdationActivity.this, "ware_house_id");
                        displayFragment.show();
                    }
                });

                button_show_item_name.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        displayFragment = new DialogDisplayFragment(ItemUpdationActivity.this, "item_name");
                        displayFragment.show();
                    }
                });

                button_show_measurement.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        displayFragment = new DialogDisplayFragment(ItemUpdationActivity.this, "measurement");
                        displayFragment.show();
                    }
                });
//        select_item_Spinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                bf.show(getSupportFragmentManager(),"data");
//            }
//        });
            }

            @Override
            public void sItemID(String ItemId) {
                String e = ItemId;
                String g = e;
                edit_text_select_item.setText(g);
            }

            @Override
            public void sWareHouseId(String WarehouseId) {
                edit_text_select_warehouse.setText(WarehouseId);
            }

            @Override
            public void sItemName(String ItemName) {
                edit_text_item_name.setText(ItemName);
            }

            @Override
            public void sMeasurementType(String MeasurementType) {
                edit_text_select_measurement.setText(MeasurementType);
            }

    @Override
    public void dismissDialog() {
        displayFragment.dismiss();
    }
}
