package com.example.mlsr.admininventoryproject.InsertInventoryItemOptions;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.mlsr.admininventoryproject.ApiServices.ApiServiceBase;
import com.example.mlsr.admininventoryproject.ConsumeInventoryItemsData.Model.WarehouseData;
import com.example.mlsr.admininventoryproject.Interfaces.APIserviceInterface;
import com.example.mlsr.admininventoryproject.Models.PojoClasses.WareHouseItemData;
import com.example.mlsr.admininventoryproject.R;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * A simple {@link Fragment} subclass.
 */
public class DialogDisplayFragment extends Dialog implements SpinnerAdapter.InterfacePassData {

    APIserviceInterface apiService;
    AddEdittextdata addEdittextdata ;
    Activity mActivity;
    String ItemId;
    String ItemName;
    String WareHouseId;
    String type;
    String MeasurementType;

    public DialogDisplayFragment(Activity mActivity,String type) {
        super(mActivity);

        this.mActivity = mActivity;
        this.addEdittextdata = (AddEdittextdata) mActivity;
        this.type = type;

    }

    RecyclerView SpinnerItemRecyclerView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.fragment_blank2);

        SpinnerItemRecyclerView = (RecyclerView) findViewById(R.id.spiner_items_recycler_view);
        SpinnerItemRecyclerView.setLayoutManager(new LinearLayoutManager(mActivity));


        LoadAllWareHouseData();
        LoadOnlyWareHouseData();


    }

    public void LoadOnlyWareHouseData(){
        apiService = ApiServiceBase.getAPIServiceInterfaceControl();

        Observable<List<WarehouseData>> observable3 = apiService.getAllWareHouseData().subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread());
        observable3.subscribe(new Observer<List<WarehouseData>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(List<WarehouseData> warehouseData) {

                ArrayList<WarehouseData> listOfWareHouseData = new ArrayList<>();
                listOfWareHouseData = (ArrayList<WarehouseData>) warehouseData;

                if(type == "ware_house_id"){
                    createDataCallAdapter(type,null,listOfWareHouseData);
                }
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });

    }


    public void LoadAllWareHouseData(){

        apiService = ApiServiceBase.getAPIServiceInterfaceControl();

        Observable<List<WareHouseItemData>> observable2 = apiService.getAllItemDataOfWareHouse().subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread());

        observable2.subscribe(new Observer<List<WareHouseItemData>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(List<WareHouseItemData> wareHouseItemData) {

                ArrayList<WareHouseItemData> listOfWareHouseData = new ArrayList<WareHouseItemData>();
                listOfWareHouseData =(ArrayList<WareHouseItemData>) wareHouseItemData;
//                SpinnerAdapter spinnerAdapter = new SpinnerAdapter(listOfWareHouseData,getContext(),DialogDisplayFragment.this);
//                SpinnerItemRecyclerView.setAdapter(spinnerAdapter);
                if(type == "item_id" || type == "item_name" || type == "measurement"){
                    createDataCallAdapter(type,listOfWareHouseData,null);
                }
            }

            @Override
            public void onError(Throwable e) {
                Log.i("error",e.getMessage());
            }

            @Override
            public void onComplete() {

            }
        });
    }

    public void createDataCallAdapter(String type,ArrayList<WareHouseItemData> listOfWareHouseData,ArrayList<WarehouseData> listOfOnlyWareHouseData){
        switch(type){
            case "item_id" :

                ArrayList<String> item_id_data = new ArrayList<>();
                item_id_data.add("ITEM ID LIST");
                for(int i=0;i<listOfWareHouseData.size();i++){
                 item_id_data.add(listOfWareHouseData.get(i).getItemID().toString());
                }
                SpinnerAdapter spinnerAdapter = new SpinnerAdapter(item_id_data,getContext(),DialogDisplayFragment.this,type);
                SpinnerItemRecyclerView.setAdapter(spinnerAdapter);
                break;

            case "ware_house_id" :

                ArrayList<String> warehouse_id_data = new ArrayList<>();
                warehouse_id_data.add(" WARE HOUSE ID LIST ");
                for(int i=0;i<listOfOnlyWareHouseData.size();i++){
                    warehouse_id_data.add(listOfOnlyWareHouseData.get(i).getWarehouseName().toString());
                }
                SpinnerAdapter spinnerAdapter1 = new SpinnerAdapter(warehouse_id_data,getContext(),DialogDisplayFragment.this,type);
                SpinnerItemRecyclerView.setAdapter(spinnerAdapter1);
                break;

            case "item_name" :

                ArrayList<String> item_name_data = new ArrayList<>();
                item_name_data.add("ITEM NAMES LIST");
                for(int i=0;i<listOfWareHouseData.size();i++){
                    item_name_data.add(listOfWareHouseData.get(i).getItemName().toString());
                }
                SpinnerAdapter spinnerAdapter2 = new SpinnerAdapter(item_name_data,getContext(),DialogDisplayFragment.this,type);
                SpinnerItemRecyclerView.setAdapter(spinnerAdapter2);
                break;

            case "measurement" :

                ArrayList<String> measurement_data = new ArrayList<>();
//                for(int i=0;i<listOfWareHouseData.size();i++){
//                    measurement_data.add(listOfWareHouseData.get(i).getMeasurementID().toString());
//                }
                measurement_data.add("MEASURENMENT TYPES");
                measurement_data.add("1");
                measurement_data.add("2");
                measurement_data.add("3");

                SpinnerAdapter spinnerAdapter3 = new SpinnerAdapter(measurement_data,getContext(),DialogDisplayFragment.this,type);
                SpinnerItemRecyclerView.setAdapter(spinnerAdapter3);
                break;
        }
    }
    @Override
    public void itemID(String name) {
        ItemId = name;
        addEdittextdata.sItemID(ItemId);
    }

    @Override
    public void setWareHouseID(String wareHouseId) {
        WareHouseId = wareHouseId;
        addEdittextdata.sWareHouseId(WareHouseId);
    }

    @Override
    public void setitemName(String itemName) {
        ItemName = itemName;
        addEdittextdata.sItemName(ItemName);
    }

    @Override
    public void setMeasurementType(String Type) {
        MeasurementType = Type;
        addEdittextdata.sMeasurementType(MeasurementType);
    }

    @Override
    public void closeDialog() {
        addEdittextdata.dismissDialog();
    }

    public interface AddEdittextdata
    {
        void sItemID(String ItemId);
        void sWareHouseId(String WarehouseId);
        void sItemName(String ItemName);
        void sMeasurementType(String MeasurementType);
        void dismissDialog();
    }
}
