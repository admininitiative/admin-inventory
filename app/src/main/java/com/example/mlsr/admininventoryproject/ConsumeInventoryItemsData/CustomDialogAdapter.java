package com.example.mlsr.admininventoryproject.ConsumeInventoryItemsData;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.mlsr.admininventoryproject.R;

import java.util.ArrayList;

/**
 * Created by MlSr on 5/22/2018.
 */

public class CustomDialogAdapter extends RecyclerView.Adapter<CustomDialogAdapter.SingleitemDialogText>{

    private Context context;
    private ArrayList<String> wareHouseValues;
    interfacePassDataToSetText interfaceDataSetText;
    String type;

    public CustomDialogAdapter(ArrayList<String> wareHouseValues,Context context, DialogDispFragment dialogDispFragment, String type) {
        this.context = context;
        this.wareHouseValues = wareHouseValues;
        this.interfaceDataSetText = dialogDispFragment;
        this.type = type;
    }

    @Override
    public SingleitemDialogText onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.spinner_item_value,parent,false);
        SingleitemDialogText singleitemDialogText = new SingleitemDialogText(v);
        return  singleitemDialogText;
    }

    @Override
    public void onBindViewHolder(SingleitemDialogText holder, final int position) {
        holder.singleDialogText.setText(wareHouseValues.get(position).toString());

        holder.singleDialogText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch(type){
                    case "item_id" : interfaceDataSetText.ItemId(wareHouseValues.get(position).toString());
                    interfaceDataSetText.CloseDialog();
                    break;

                    case "ware_house_id_name" : interfaceDataSetText.WareHouseId(wareHouseValues.get(position).toString());
                    interfaceDataSetText.CloseDialog();
                    break;

                    case "item_name" : interfaceDataSetText.ItemName(wareHouseValues.get(position).toString());
                    interfaceDataSetText.CloseDialog();
                    break;

                    case "measurement" : interfaceDataSetText.MeasurementType(wareHouseValues.get(position).toString());
                    interfaceDataSetText.CloseDialog();
                    break;

                    default :
                        break;
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return wareHouseValues.size();
    }

    public class SingleitemDialogText extends RecyclerView.ViewHolder{

        private TextView singleDialogText;
        public SingleitemDialogText(View itemView) {

            super(itemView);
            singleDialogText = (TextView) itemView.findViewById(R.id.spinner_display_id);

        }
    }

    public interface interfacePassDataToSetText {

        public void ItemId(String itemId);
        public void WareHouseId(String wareHouseId);
        public void ItemName(String itemName);
        public void MeasurementType(String measurementType);
        public void CloseDialog();

    }
}
