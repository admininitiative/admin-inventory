package com.example.mlsr.admininventoryproject.ConsumeInventoryItemsData;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mlsr.admininventoryproject.ApiServices.ApiServiceBase;
import com.example.mlsr.admininventoryproject.ConsumeInventoryItemsData.Model.GetItemByQRCode;
import com.example.mlsr.admininventoryproject.InsertInventoryItemOptions.DialogDisplayFragment;
import com.example.mlsr.admininventoryproject.Interfaces.APIserviceInterface;
import com.example.mlsr.admininventoryproject.LoginPage.LoginActivity;
import com.example.mlsr.admininventoryproject.MainActivity;
import com.example.mlsr.admininventoryproject.Models.PojoClasses.LoginData;
import com.example.mlsr.admininventoryproject.Models.PojoClasses.UpdateWareHouseItem;
import com.example.mlsr.admininventoryproject.Models.PojoClasses.WareHouseItemData;
import com.example.mlsr.admininventoryproject.R;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

public class ConsumeItemsData extends AppCompatActivity implements DialogDispFragment.interfacePassDataToConsumeItemsData{

    Button ConsumeButton;
    EditText editTextSelectItemId;
    EditText editTextSelectWareHouseIdName;
    EditText editTextEnterQuanity;
    EditText editTextSelectMeasurement;
    EditText editTextItemName;
    ImageButton measurementsDropDown;
    ImageButton ItemIdDropDown;
    ImageButton wareHouseIdDropDown;
    ImageButton itemNameDropdown;
    DialogDispFragment displayFragment;

    APIserviceInterface apiService;
    Dialog myDialog;
    TextView logoutTextView;

    ImageView ImageViewClick;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consume_items_data);

         editTextSelectItemId = (EditText) findViewById(R.id.consume_item_select_item_id);
         editTextSelectWareHouseIdName = (EditText) findViewById(R.id.consume_item_select_warehouse_id_name);
         editTextItemName = (EditText) findViewById(R.id.consume_item_item_name);
         editTextSelectMeasurement = (EditText) findViewById(R.id.consume_item_measurement_id);
         editTextEnterQuanity = (EditText) findViewById(R.id.item_updation_item_quantity);
         ConsumeButton = (Button) findViewById(R.id.consume_items_button);
         ImageViewClick = (ImageView) findViewById(R.id.consume_item_click_profile);


         myDialog = new Dialog(this);
         myDialog.setContentView(R.layout.custom_pop_up);
         logoutTextView = (TextView) myDialog.findViewById(R.id.logout_button_text_view);


        measurementsDropDown = (ImageButton) findViewById(R.id.select_measurement_drop_down);
        ItemIdDropDown = (ImageButton) findViewById(R.id.select_item_id_drop_down);
        wareHouseIdDropDown = (ImageButton) findViewById(R.id.select_warehouse_id_drop_down);
        itemNameDropdown = (ImageButton) findViewById(R.id.select_item_name_drop_down);

        apiService = ApiServiceBase.getAPIServiceInterfaceControl();

        spinnerOnClickListeners();
        consumeButtonClick();
        profilePicLogoutButtonClick();
        getAllItemsByQRparameter();

    }

    public void getAllItemsByQRparameter(){

        String WareHouseName = "1B1029";
        String WareHouseAddress = "B Wings , 1st floor, Old Building, Pune";

        Observable<List<GetItemByQRCode>> ObservableData = apiService.getItemByQRparameter(WareHouseName,WareHouseAddress).subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread());
        ObservableData.subscribe(new Observer<List<GetItemByQRCode>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(List<GetItemByQRCode> getItemByQRCodes) {
                editTextSelectItemId.setText(getItemByQRCodes.get(0).getItemData().getItemID().toString());
                editTextItemName.setText(getItemByQRCodes.get(0).getItemData().getItemName().toString());
                editTextEnterQuanity.setText("10");
                editTextSelectWareHouseIdName.setText(getItemByQRCodes.get(0).getWarehouseData().getWarehouseID().toString());
                editTextSelectMeasurement.setText(getItemByQRCodes.get(0).getMeasurementData().getUnitType().toString());
                Toast.makeText(getApplicationContext(), "it is working fine", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(Throwable e) {
                Toast.makeText(getApplicationContext(),"error in consume activity",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onComplete() {

            }
        });

    }

    public void  profilePicLogoutButtonClick(){
        logoutTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
                Intent intent = new Intent(ConsumeItemsData.this, LoginActivity.class);
                startActivity(intent);
            }
        });

        ImageViewClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.show();
            }
        });
    }


    public void consumeButtonClick(){
        ConsumeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String TempWareHouseIdName = editTextSelectWareHouseIdName.getText().toString();
                String TempItemId = editTextSelectItemId.getText().toString();
                String TempItemName = editTextItemName.getText().toString();
                String TempItemQty = editTextEnterQuanity.getText().toString();
                String TempMeasurement = editTextSelectMeasurement.getText().toString();

                if( TempItemId.isEmpty() && !TempWareHouseIdName.isEmpty() && !TempItemName.isEmpty()
                        && !TempItemQty.isEmpty() && !TempMeasurement.isEmpty()) {

                int itemId = Integer.parseInt(editTextSelectItemId.getText().toString());
                String wareHouseidName = editTextSelectWareHouseIdName.getText().toString();
                String itemName = editTextItemName.getText().toString();
                int itemQty = Integer.parseInt(editTextEnterQuanity.getText().toString());
                int measurement = Integer.parseInt(editTextSelectMeasurement.getText().toString());
                int wareHouseId = 10057;

                    UpdateWareHouseItem updateWareHouseItem = new UpdateWareHouseItem(itemId,wareHouseId,wareHouseidName,itemName,itemQty,measurement,"box",false,true);

                    io.reactivex.Observable<Response<String>> observable = apiService.UpdateItemsToWareHouse(updateWareHouseItem).subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread());
                    observable.subscribe(new Observer<Response<String>>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(Response<String> stringResponse) {
                            Toast.makeText(getApplicationContext(),stringResponse.message().toString(),Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onError(Throwable e) {

                        }

                        @Override
                        public void onComplete() {

                        }
                    });
                }
                else {

                    Toast.makeText(getApplicationContext()," please fill all fields",Toast.LENGTH_SHORT).show();
                }
//                int itemId = (Integer.parseInt(editTextSelectItemId.getText().toString()) != 0) ? (Integer.parseInt(editTextSelectItemId.getText().toString())) : 10005;
//                int wareHouseid = (Integer.parseInt(editTextSelectWareHouseIdName.getText().toString()) != 0) ? Integer.parseInt(editTextSelectWareHouseIdName.getText().toString()) : 45;
//                String itemName = (editTextItemName.getText().toString() != null) ? editTextItemName.getText().toString() : "bags";
//                int itemQty = (Integer.parseInt(editTextEnterQuanity.getText().toString()) != 0) ? Integer.parseInt(editTextEnterQuanity.getText().toString()) : 10;
//                int measurement = (Integer.parseInt(editTextSelectMeasurement.getText().toString()) != 0 ) ?Integer.parseInt(editTextSelectMeasurement.getText().toString()) : 1;

//                if(itemId != 0 && wareHouseid != 0 && itemName != null && itemQty != 0 && measurement !=  0) {

             //   }
//                else{
//                    Toast.makeText(getApplicationContext(),"please fill the consume data completely",Toast.LENGTH_SHORT).show();
//                }
            }
        });
    }


    public void spinnerOnClickListeners(){

        measurementsDropDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                displayFragment = new DialogDispFragment(ConsumeItemsData.this,"measurement");
                displayFragment.show();
            }
        });

        ItemIdDropDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                displayFragment = new DialogDispFragment(ConsumeItemsData.this,"item_id");
                displayFragment.show();

            }
        });

        wareHouseIdDropDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayFragment = new DialogDispFragment(ConsumeItemsData.this,"ware_house_id_name");
                displayFragment.show();
            }
        });

        itemNameDropdown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayFragment = new DialogDispFragment(ConsumeItemsData.this,"item_name");
                displayFragment.show();
            }
        });
    }

    @Override
    public void setItemId(String itemId) {
        editTextSelectItemId.setText(itemId);
    }

    @Override
    public void setWareHouseId(String wareHouseId) {
        editTextSelectWareHouseIdName.setText(wareHouseId);
    }

    @Override
    public void setItemName(String itemName) {
        editTextItemName.setText(itemName);
    }

    @Override
    public void setMeasurementType(String measurementType) {
        editTextSelectMeasurement.setText(measurementType);
    }

    @Override
    public void closeDialog(){
        displayFragment.dismiss();
    }
}
