
package com.example.mlsr.admininventoryproject.ConsumeInventoryItemsData.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MeasurementData {

    @SerializedName("MeasurementID")
    @Expose
    private Integer measurementID;
    @SerializedName("UnitType")
    @Expose
    private String unitType;
    @SerializedName("SubUnitType")
    @Expose
    private Object subUnitType;
    @SerializedName("PageNumber")
    @Expose
    private Integer pageNumber;

    public Integer getMeasurementID() {
        return measurementID;
    }

    public void setMeasurementID(Integer measurementID) {
        this.measurementID = measurementID;
    }

    public String getUnitType() {
        return unitType;
    }

    public void setUnitType(String unitType) {
        this.unitType = unitType;
    }

    public Object getSubUnitType() {
        return subUnitType;
    }

    public void setSubUnitType(Object subUnitType) {
        this.subUnitType = subUnitType;
    }

    public Integer getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(Integer pageNumber) {
        this.pageNumber = pageNumber;
    }

}
