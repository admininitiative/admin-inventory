package com.example.mlsr.admininventoryproject.Models.PojoClasses;

/**
 * Created by MlSr on 4/24/2018.
 */


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WareHouseDetails {

    @SerializedName("WarehouseID")
    @Expose
    private Integer warehouseID;
    @SerializedName("WarehouseName")
    @Expose
    private String warehouseName;
    @SerializedName("Address")
    @Expose
    private String address;
    @SerializedName("QRCode")
    @Expose
    private Object qRCode;
    @SerializedName("IsActive")
    @Expose
    private Boolean isActive;
    @SerializedName("PageNumber")
    @Expose
    private Integer pageNumber;


    public WareHouseDetails(Integer warehouseID, String warehouseName, String address, Object qRCode, Boolean isActive, Integer pageNumber) {
        this.warehouseID = warehouseID;
        this.warehouseName = warehouseName;
        this.address = address;
        this.qRCode = qRCode;
        this.isActive = isActive;
        this.pageNumber = pageNumber;
    }


    public Integer getWarehouseID() {
        return warehouseID;
    }

    public void setWarehouseID(Integer warehouseID) {
        this.warehouseID = warehouseID;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Object getQRCode() {
        return qRCode;
    }

    public void setQRCode(Object qRCode) {
        this.qRCode = qRCode;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Integer getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(Integer pageNumber) {
        this.pageNumber = pageNumber;
    }
}