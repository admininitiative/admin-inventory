package com.example.mlsr.admininventoryproject.DashboardPage;


import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.mlsr.admininventoryproject.Models.InventoryItemsDataList;
import com.example.mlsr.admininventoryproject.Models.PojoClasses.WareHouseItemData;
import com.example.mlsr.admininventoryproject.R;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by MlSr on 4/13/2018.
 */

public class InventoryRecordListAdapter extends RecyclerView.Adapter<InventoryRecordListAdapter.SingleInventoryRecordListHolder> {

//    HashMap<String,ArrayList<InventoryItemsDataList>> locationWiseData;
    Context context;
    ArrayList<WareHouseItemData> listOfWareHouseItemData;
    int warehouseId;

    public InventoryRecordListAdapter(Context context, ArrayList<WareHouseItemData> listOfWareHouseItemData,int wareHouseId) {
        this.context = context;
        this.listOfWareHouseItemData = listOfWareHouseItemData;
        this.warehouseId = wareHouseId;
    }
//    public InventoryRecordListAdapter(HashMap<String, ArrayList<InventoryItemsDataList>> locationWiseData, Context context) {
//        this.locationWiseData = locationWiseData;
//        this.context = context;
//    }

    @Override
    public SingleInventoryRecordListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_inventory_record_item,parent,false);
        SingleInventoryRecordListHolder singleInventoryRecordListHolder = new SingleInventoryRecordListHolder(v);
        return singleInventoryRecordListHolder;
    }

    @Override
    public void onBindViewHolder(SingleInventoryRecordListHolder holder, int position) {
//       if( listOfWareHouseItemData.get(position).getWarehouseID() == warehouseId ){
           holder.productTextView.setText(listOfWareHouseItemData.get(position).getItemName().toString());
           holder.quantityTextView.setText(listOfWareHouseItemData.get(position).getQuantity().toString());
//       }
//       else{
//           holder.productTextView.setText("");
//           holder.quantityTextView.setText("");
//           holder.recievedByTextView.setText("");
//           holder.locationtextView.setText("");
//       }
//        ArrayList<InventoryItemsDataList> inventoryItemsDataLists = new ArrayList<>();
//        switch(position){
//            case 0 : inventoryItemsDataLists = locationWiseData.get("Bangalore");
//            break;
//
//            case 1 : inventoryItemsDataLists = locationWiseData.get("Chennai");
//            break;
//
//            case 2 : inventoryItemsDataLists = locationWiseData.get("Pune");
//            break;
//
//            case 3 : inventoryItemsDataLists = locationWiseData.get("Hyderabad");
//            break;
//        }
//        for(int i=0 ; i < inventoryItemsDataLists.size();i++){
//            holder.productTextView.setText(inventoryItemsDataLists.get(i).getProductName().toString());
//            holder.quantityTextView.setText(Integer.toString(inventoryItemsDataLists.get(i).getQuantity()));
//            holder.locationtextView.setText(inventoryItemsDataLists.get(i).getLocation());
//            holder.recievedByTextView.setText(inventoryItemsDataLists.get(i).getRecievedBy());
//        }
    }

    @Override
    public int getItemCount() {
//        return locationWiseData.size();
        return listOfWareHouseItemData.size();
    }

    public class SingleInventoryRecordListHolder extends RecyclerView.ViewHolder{

        TextView productTextView;
        TextView quantityTextView;
        TextView locationtextView;
        TextView recievedByTextView;

        public SingleInventoryRecordListHolder(View itemView) {
            super(itemView);
            productTextView = (TextView) itemView.findViewById(R.id.product_text_view);
            quantityTextView = (TextView) itemView.findViewById(R.id.quantity_text_view);
            locationtextView = (TextView) itemView.findViewById(R.id.location_text_view);
            recievedByTextView = (TextView) itemView.findViewById(R.id.recieved_by_text_view);
        }
    }
}
