package com.example.mlsr.admininventoryproject.DashboardPage;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.mlsr.admininventoryproject.ApiServices.ApiServiceBase;
import com.example.mlsr.admininventoryproject.Interfaces.APIserviceInterface;
import com.example.mlsr.admininventoryproject.LoginPage.LoginActivity;
import com.example.mlsr.admininventoryproject.Models.InventoryItemsDataList;
import com.example.mlsr.admininventoryproject.Models.PojoClasses.WareHouseDetails;
import com.example.mlsr.admininventoryproject.Models.PojoClasses.WareHouseItemData;
import com.example.mlsr.admininventoryproject.Models.TempData;
import com.example.mlsr.admininventoryproject.QRcodeImageScanner.ScanQrCodeReturnData;
import com.example.mlsr.admininventoryproject.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class DashboardActivity extends AppCompatActivity implements WareHouseDataListAdapter.WarehouseData {

//    private ActionBar toolbar;

    ArrayList<String> scannedBarcodesData;
    private Context context;
    int WareHouseId ;


    @BindView(R.id.DashboardProgressBar)
    ProgressBar DashBoardProgress;

    HashMap<String,ArrayList<InventoryItemsDataList>> hm ;
    ArrayList<InventoryItemsDataList> inventoryItemsDataLists;
    RecyclerView location_wise_inventory_details ;
    ArrayList<WareHouseItemData> arrayListOfWareHouseData;


    RecyclerView location_wise_ware_house_data;


    ImageButton addInventory ;
    ImageButton ConsumeInventory;
    APIserviceInterface apiService;
    Dialog myDialog;
    TextView logoutTextView;

    LinearLayout addinventorylinearLayout;
    LinearLayout consumeInventoryLinearLayout;

    ImageView ProfileImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context = this;

        myDialog = new Dialog(this);
        myDialog.setContentView(R.layout.custom_pop_up);


        logoutTextView = (TextView) myDialog.findViewById(R.id.logout_button_text_view);

        scannedBarcodesData = new ArrayList<String>();
        setContentView(R.layout.activity_dashboard);

        ButterKnife.bind(this);

        DashBoardProgress.setVisibility(View.GONE);

        Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);

        addinventorylinearLayout = (LinearLayout) findViewById(R.id.addInventory_parent);
        consumeInventoryLinearLayout = (LinearLayout) findViewById(R.id.parent_consume_item);
        ProfileImageView = (ImageView) findViewById(R.id.profile_imageView);

        location_wise_inventory_details = (RecyclerView) findViewById(R.id.location_wise_inventory_details);

        location_wise_ware_house_data = (RecyclerView) findViewById(R.id.location_wise_warehouse_details);

        addInventory = (ImageButton) findViewById(R.id.add_Inventory_Button);
        ConsumeInventory = (ImageButton) findViewById(R.id.consume_Inventory_Button);


        Toolbar top_toolbar = (Toolbar) findViewById(R.id.top_toolbar);
        top_toolbar.setTitle("DASHBOARD");

        setSupportActionBar(toolbar);

         apiService = ApiServiceBase.getAPIServiceInterfaceControl();

        location_wise_ware_house_data.setHasFixedSize(true);
//        location_wise_ware_house_data.setLayoutManager(new GridLayoutManager(this,2));

        location_wise_ware_house_data.setLayoutManager(new LinearLayoutManager(this));

        inventoryItemsDataLists = new ArrayList<>();
        TempData tempData = new TempData();

        hm = new HashMap<>();

        hm.put("Bangalore",tempData.fixedLocationData());
        hm.put("Chennai",tempData.fixedLocationData());
        hm.put("Pune",tempData.fixedLocationData());
        hm.put("Hyderabad",tempData.fixedLocationData());

        LoadWareHouseData();
        addInventoryItem();
        consumeInventoryItem();
        collectDataAndDisplayData();
        profileClickAndLogoutButton();

    }
    public void profileClickAndLogoutButton(){
        ProfileImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.show();
            }
        });

        logoutTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
                Intent intent = new Intent(DashboardActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });
    }

    public void inventoriesOfWareHouse(){

//        APIserviceInterface apiService1 = ApiServiceBase.getAPIServiceInterfaceControl();
        Observable<List<WareHouseItemData>> observable1 = apiService.getAllItemDataOfWareHouse().subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread());

        observable1.subscribe(new Observer<List<WareHouseItemData>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(List<WareHouseItemData> wareHouseItemData) {
                arrayListOfWareHouseData = new ArrayList<>();
                ArrayList<WareHouseItemData> listOfWareHouseData = new ArrayList<WareHouseItemData>();
                    listOfWareHouseData =(ArrayList<WareHouseItemData>) wareHouseItemData;
                for(int i=0;i< listOfWareHouseData.size();i++){
                    if(listOfWareHouseData.get(i).getWarehouseID() == WareHouseId) {
                        arrayListOfWareHouseData.add(listOfWareHouseData.get(i));
                    }
                }
                location_wise_inventory_details.setHasFixedSize(true);
                location_wise_inventory_details.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                InventoryRecordListAdapter inventoryRecordListAdapter = new InventoryRecordListAdapter(getApplicationContext(),arrayListOfWareHouseData,WareHouseId);
                location_wise_inventory_details.setAdapter(inventoryRecordListAdapter);
            }

            @Override
            public void onError(Throwable e) {
                Log.i("error",e.getMessage().toString());
            }

            @Override
            public void onComplete() {
                Log.i("error","error");
//                arrayListOfWareHouseData.clear();
            }
        });
    }

    public void LoadWareHouseData(){

//        new Thread(new Runnable() {
//            @Override
//            public void run() {



                DashBoardProgress.setVisibility(View.VISIBLE);

                Observable<List<WareHouseDetails>> observable = apiService.getWareHouseDetails().subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread());

                observable.subscribe(new Observer<List<WareHouseDetails>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(List<WareHouseDetails> wareHouseDetails) {

                        ArrayList<WareHouseDetails> ListOfWareHouseData = new ArrayList<>();
                        ListOfWareHouseData =(ArrayList<WareHouseDetails>) wareHouseDetails;
                        WareHouseDataListAdapter wareHouseDataListAdapter = new WareHouseDataListAdapter(getApplicationContext(),DashboardActivity.this,ListOfWareHouseData);
                        location_wise_ware_house_data.setAdapter(wareHouseDataListAdapter);
                        DashBoardProgress.setVisibility(View.GONE);

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.i("error",e.getMessage().toString());
                    }

                    @Override
                    public void onComplete() {
                        Log.i("error","some error");
                    }
                });
//            }
//        }).start();
//        Thread.dumpStack();


    }

    public void collectDataAndDisplayData(){

       final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.custom_dialog_box_name);
        TextView dialogTextView = (TextView) dialog.findViewById(R.id.CustomDialog_inventory_EditText);
        scannedBarcodesData = getIntent().getStringArrayListExtra("BarcodeData");
        if(scannedBarcodesData != null) {

            dialog.show();
            dialogTextView.setText(scannedBarcodesData.get(1));

        }
        ImageButton DismissDialogButton = (ImageButton) dialog.findViewById(R.id.image_stop_button);

        DismissDialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    public void addInventoryItem(){
        addInventory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DashBoardProgress.setVisibility(View.VISIBLE);
                Intent intent = new Intent(DashboardActivity.this, ScanQrCodeReturnData.class);
                intent.putExtra("ItemType","add Item");
                startActivity(intent);
                DashBoardProgress.setVisibility(View.GONE);
//                finish();
            }
        });
        addinventorylinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DashboardActivity.this, ScanQrCodeReturnData.class);
                intent.putExtra("ItemType","add Item");
                startActivity(intent);
            }
        });
    }

    public void consumeInventoryItem(){
        ConsumeInventory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DashboardActivity.this,ScanQrCodeReturnData.class);
                intent.putExtra("ItemType","Consume Item");
                startActivity(intent);
            }
        });

        consumeInventoryLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DashboardActivity.this,ScanQrCodeReturnData.class);
                intent.putExtra("ItemType","Consume Item");
                startActivity(intent);
            }
        });
    }

    @Override
    public void getWarehouseID(int warehouseID) {

        WareHouseId = warehouseID;
        inventoriesOfWareHouse();

    }
}
