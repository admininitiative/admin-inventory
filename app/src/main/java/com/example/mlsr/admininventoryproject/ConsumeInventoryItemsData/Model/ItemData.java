
package com.example.mlsr.admininventoryproject.ConsumeInventoryItemsData.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ItemData {

    @SerializedName("itemID")
    @Expose
    private Integer itemID;
    @SerializedName("WarehouseID")
    @Expose
    private Integer warehouseID;
    @SerializedName("WarehouseName")
    @Expose
    private Object warehouseName;
    @SerializedName("ItemName")
    @Expose
    private String itemName;
    @SerializedName("Quantity")
    @Expose
    private Integer quantity;
    @SerializedName("MeasurementID")
    @Expose
    private Integer measurementID;
    @SerializedName("UnitType")
    @Expose
    private Object unitType;
    @SerializedName("AddItem")
    @Expose
    private Boolean addItem;
    @SerializedName("SubtractItem")
    @Expose
    private Boolean subtractItem;

    public Integer getItemID() {
        return itemID;
    }

    public void setItemID(Integer itemID) {
        this.itemID = itemID;
    }

    public Integer getWarehouseID() {
        return warehouseID;
    }

    public void setWarehouseID(Integer warehouseID) {
        this.warehouseID = warehouseID;
    }

    public Object getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(Object warehouseName) {
        this.warehouseName = warehouseName;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getMeasurementID() {
        return measurementID;
    }

    public void setMeasurementID(Integer measurementID) {
        this.measurementID = measurementID;
    }

    public Object getUnitType() {
        return unitType;
    }

    public void setUnitType(Object unitType) {
        this.unitType = unitType;
    }

    public Boolean getAddItem() {
        return addItem;
    }

    public void setAddItem(Boolean addItem) {
        this.addItem = addItem;
    }

    public Boolean getSubtractItem() {
        return subtractItem;
    }

    public void setSubtractItem(Boolean subtractItem) {
        this.subtractItem = subtractItem;
    }

}
