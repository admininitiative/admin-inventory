package com.example.mlsr.admininventoryproject.Models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MlSr on 4/13/2018.
 */

public class TempData {

    ArrayList<InventoryItemsDataList> ListOfInventoryItemsDataLists ;

    public ArrayList<InventoryItemsDataList> fixedLocationData(){
        ListOfInventoryItemsDataLists = new ArrayList<>();
        ListOfInventoryItemsDataLists.add(new InventoryItemsDataList("Book",100,"Bangalore","sreekar"));
        ListOfInventoryItemsDataLists.add(new InventoryItemsDataList("pen",150,"Chennai","shabber"));

        return ListOfInventoryItemsDataLists;
    }
}
