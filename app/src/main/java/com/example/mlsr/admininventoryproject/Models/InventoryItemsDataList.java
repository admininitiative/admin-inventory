package com.example.mlsr.admininventoryproject.Models;

/**
 * Created by MlSr on 4/13/2018.
 */

public class InventoryItemsDataList {
    public String ProductName;
    public int Quantity;
    public String Location;
    public String RecievedBy;

    public InventoryItemsDataList(String productName, int quantity, String location, String recievedBy) {
        ProductName = productName;
        Quantity = quantity;
        Location = location;
        RecievedBy = recievedBy;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public int getQuantity() {
        return Quantity;
    }

    public void setQuantity(int quantity) {
        Quantity = quantity;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String location) {
        Location = location;
    }

    public String getRecievedBy() {
        return RecievedBy;
    }

    public void setRecievedBy(String recievedBy) {
        RecievedBy = recievedBy;
    }
}
